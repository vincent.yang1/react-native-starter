#!/usr/bin/env bash

# Example: Change bundle name of an iOS app for non-production

if [ "$APPCENTER_ENV" == "beta" ];
then
    echo API_ENV = beta >> .env
fi

if [ "$APPCENTER_ENV" == "pre" ];
then
    echo API_ENV = pre >> .env
fi
if [ "$APPCENTER_ENV" == "prod" ];
then
    echo API_ENV = prod >> .env
fi
echo BUGSNAG_KEY = $BUGSNAG_KEY >> .env

ANDROID_BUILD_GRADLE_FILE=$APPCENTER_SOURCE_DIRECTORY/android/app/build.gradle
ANDROID_BUILD_VALUES_STRINGS=$APPCENTER_SOURCE_DIRECTORY/android/app/src/main/res/values/strings.xml

INFO_PLIST_FILE=$APPCENTER_SOURCE_DIRECTORY/ios/$PROJECT_NAME/Info.plist


if [ "$APPCENTER_PLATFORM" == "android" ]
then
	
	APPCENTER_CONFIG_JSON_FILE=$APPCENTER_SOURCE_DIRECTORY/android/app/src/main/assets/appcenter-config.json

	echo "Updating appcenter-config.json"
	sed -i -e 's/$APPCENTER_APP_SECRET/'"$APPCENTER_APP_SECRET"'/g'   $APPCENTER_CONFIG_JSON_FILE
	echo "File appcenter-config.json content:"
	cat $APPCENTER_CONFIG_JSON_FILE

    echo "Updating version name to $VERSION_NAME in build.gradle"
	sed -i '' 's/versionName "[0-9.]*"/versionName "'$VERSION_NAME'"/' $ANDROID_BUILD_GRADLE_FILE

    echo "Updating version code to $APPCENTER_BUILD_ID in build.gradle"
	sed -i '' 's/versionCode [0-9.]/versionCode '$APPCENTER_BUILD_ID'/' $ANDROID_BUILD_GRADLE_FILE


	echo "Updating applicationId to $PROJECT_NAME_$APPCENTER_ENV in build.gradle"
	sed -i '' 's/applicationId "com.starter"/applicationId "com.'$PROJECT_NAME'_'$APPCENTER_ENV'"/' $ANDROID_BUILD_GRADLE_FILE
	if [ "$APPCENTER_ENV" != "prod" ]
	then
		echo "Updating appname to $PROJECT_NAME_$APPCENTER_ENV in values/strings.xml"

		sed -i '' 's/starter/'$PROJECT_NAME'_'$APPCENTER_ENV'/' $ANDROID_BUILD_VALUES_STRINGS
  		
  		echo "Updating version name to $VERSION_NAME.$APPCENTER_BUILD_ID in build.gradle"
		sed -i '' 's/versionName "[0-9.]*"/versionName "'$VERSION_NAME'.'$APPCENTER_BUILD_ID'"/' $ANDROID_BUILD_GRADLE_FILE


	fi



	echo "File build.gradle content:"
    cat $ANDROID_BUILD_GRADLE_FILE

	echo "File values/strings.xml content:"
    cat $ANDROID_BUILD_VALUES_STRINGS
else
    #iOS
    CONFIG_FILE="AppCenter-Config.plist"
	APPCENTER_CONFIG_JSON_FILE=$APPCENTER_SOURCE_DIRECTORY/ios/$CONFIG_FILE

	echo "Updating appcenter-config.json"
	sed -i -e 's/$APPCENTER_APP_SECRET/'"$APPCENTER_APP_SECRET"'/g'   $APPCENTER_CONFIG_JSON_FILE
	echo "File $CONFIG_FILE content:"
	cat $APPCENTER_CONFIG_JSON_FILE


    echo "Updating versionName versionCode appName to $VERSION_NAME in Info.plist"
    plutil -replace CFBundleShortVersionString -string $VERSION_NAME $INFO_PLIST_FILE
    plutil -replace CFBundleDisplayName -string $PROJECT_NAME-$APPCENTER_ENV $INFO_PLIST_FILE
    plutil -replace CFBundleVersion -string $APPCENTER_BUILD_ID $INFO_PLIST_FILE




	if [ "$APPCENTER_ENV" != "prod" ]
	then

  		
  		echo "Updating version name to $VERSION_NAME.$APPCENTER_BUILD_ID in build.gradle"
    	plutil -replace CFBundleShortVersionString -string $VERSION_NAME.$APPCENTER_BUILD_ID $INFO_PLIST_FILE


	fi

	echo "File Info.plist content:"
	cat $INFO_PLIST_FILE
	
fi

echo test



