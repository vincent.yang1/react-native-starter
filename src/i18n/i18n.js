import I18n from 'react-native-i18n';
import en from './locales/en';
import zh from './locales/zh';

I18n.fallbacks = true;
I18n.locale = 'zh';

I18n.translations = {
  en,
  zh
};

export default I18n;