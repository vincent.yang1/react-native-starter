import * as types from '../actions/ActionTypes';

const initialState = {
    userAssets: {}
};

const HomeReducer = (state = initialState, action) => {

console.log(action);
    switch (action.type) {
        //获取全部
        case types.FETCH_MYACCOUNT_DATA:
            return Object.assign({}, state, {userLoading: false, isLogin:action.isLogin, userAssets: action.userAssets});
        default:
            return state;
    }
}
export default HomeReducer;