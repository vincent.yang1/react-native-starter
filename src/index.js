
import React, {Component} from 'react';

import {
    AppRegistry,
    View,
    Text,
    TextInput,

} from 'react-native';

import {Provider} from 'react-redux';
import { TopView } from 'teaset';

import createStore from './store';

import {AppNavigator} from './navigationConfiguration';

let store = createStore().store
Text.allowFontScaling = false;
TextInput.allowFontScaling = false;
console.disableYellowBox = true;

//发布时屏蔽掉所有的console.*调用
if (!__DEV__) {

    global.console = {
        info: () => {},
        log: () => {},
        assert: () => {},
        warn: () => {},
        debug: () => {},
        error: () => {},
        time: () => {},
        timeEnd: () => {}
    };
}

console.log(createStore())

class Index extends Component {


    render() {
        const prefix = Platform.OS == 'android' ? 'dipbit://dipbit/' : 'dipbit://';

        return (
            <View style={{flex: 1}}>
                <Provider store={store}>
                    <TopView>
                        <AppNavigator  uriPrefix={prefix} />
                    </TopView>
                </Provider>

            </View>
        );
    }


}


AppRegistry.registerComponent('starter', () => Index);