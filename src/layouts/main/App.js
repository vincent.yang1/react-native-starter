/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Button,
    Dimensions,
    Text,
    NativeModules,
    Alert,
    ActivityIndicator,
    PixelRatio,
    TouchableOpacity,
    Image,
    StatusBar,
} from 'react-native';
import FastImage from 'react-native-fast-image'
import ContentLoader, { Rect, Circle } from 'react-content-loader/native'
import RNThumbnail from 'react-native-thumbnail';
import { Label, Input } from 'teaset';
import Toast, { DURATION } from 'react-native-easy-toast'
import { API_ENV, BUGSNAG_KEY } from 'react-native-dotenv';
const { MobLogin } = NativeModules
console.log(MobLogin)
console.log(API_ENV)
console.log(BUGSNAG_KEY)
// import VideoPlayer from 'react-native-video-controls';
import ImagePicker from 'react-native-image-picker';
import VideoPlayer from 'react-native-video-player';
import AliyunOSS from 'aliyun-oss-react-native'
import LottieView from 'lottie-react-native';
import { Client } from 'bugsnag-react-native';
let bugsnag
if (__DEV__) {
    console.log('Development');
} else {
    bugsnag = new Client(BUGSNAG_KEY);
}


// const config = require("./oss_property.json")
AliyunOSS.enableDevMode();
import Demo from '../demo/Demo'
import ImageList from '../demo/ImageList'
import I18n from '../../i18n/i18n';
const VIMEO_ID = '179859217';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

let thumbnail = ""
let contentOffsetY = 0
import {
    Colors
} from 'react-native/Libraries/NewAppScreen'


export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: "",
            video: { width: undefined, height: undefined, duration: undefined },
            thumbnail: undefined,
            fullscreen: false,
            showLottie: true
        }
    }
    _onPressShare(type) {
        if (this.state.shareing) true;
        this.setState({
            shareing: true
        })

        let platform = ""
        if (Platform.OS === "ios") {
            switch (type) {
                case "wechat":
                    platform = 22;
                    break;
                case "weibo":
                    platform = 1;
                    break;
                case "twitter":
                    platform = 11;
                    break;
                case "facebook":
                    platform = 10;
                    break;
                case "qq":
                    platform = 24;
                    break;


            }
        } else {
            switch (type) {
                case "wechat":
                    platform = "Wechat";
                    break;
                case "weibo":
                    platform = "SinaWeibo";
                    break;
                case "twitter":
                    platform = "Twitter";
                    break;
                case "facebook":
                    platform = "Facebook";
                    break;
                case "qq":
                    platform = "QQ";
                    break;

            }
        }

        MobLogin.showShare(platform, '我是标题', '分享什么内容', 'http://f1.sharesdk.cn/', 'http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg', (error, someData) => {

            if (error) {
                console.log(error)
                this.refs.toast.show(error);
            } else {
                console.log(someData)
                this.refs.toast.show(someData);
                this.setState({
                    shareing: false
                })

            }
        })
    }

    componentDidMount() {
        let that = this
        const configuration = {
            maxRetryCount: 3,
            timeoutIntervalForRequest: 30,
            timeoutIntervalForResource: 24 * 60 * 60
        };
        // AliyunOSS.initWithPlainTextAccessKey(config.accessKeyId, config.accessKeySecret, 'oss-cn-hangzhou.aliyuncs.com', configuration);
        console.log("initAKSK success!")
        setTimeout(function() {
            that.setState({
                showLottie: false
            })
        }, 1000)

        this.refs.toast.show('hello world!', 500, () => {
            // something you want to do at close
        });

    }
    handleScroll = (event) => {
        try {
            console.log(event.nativeEvent.contentOffset.y);
            this.setState({
                contentOffsetY: event.nativeEvent.contentOffset.y
            })
        } catch (e) {
            console.log(e)
        }

    }

    onToggleFullScreen = () => {


        this.setState({
            fullscreen: !this.state.fullscreen
        }, function() {})




    }

    onEnd = () => {
        this.setState({
            fullscreen: false,
        }, function() {})


    }

    selectVideoTapped = () => {
        let that = this
        const options = {
            title: 'Video Picker',
            takePhotoButtonTitle: null,
            mediaType: 'video',
            // videoQuality: 'medium',
            // durationLimit: 5,
            // storageOptions: {
            //   path: 'videos',
            //   skipBackup: true
            // }
        };

        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled video picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = {
                    uri: Platform.OS === 'ios' ? response.uri : response.path,
                    name: response.fileName
                };

                that.setState({
                    videoSource: null
                })





                AliyunOSS.asyncUpload("gwave-beta", "oss_demo/" + response.fileName, source.uri).then((res) => {
                    console.log(res)
                    let upload_path = 'https://' + "gwave-beta" + ".oss-cn-hangzhou.aliyuncs.com" + '/' + "oss_demo/" + response.fileName
                    console.log(upload_path)


                    that.setState({
                        videoSource: upload_path,
                    });


                    Alert.alert(
                        'Alert Title',
                        "恭喜你成功上传到阿里云服务器",
                        [
                            { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
                            { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                            { text: 'OK', onPress: () => console.log('OK Pressed') },
                        ], { cancelable: false }
                    )
                })


                // RNThumbnail.get(Platform.OS === 'ios' ? response.uri : response.path).then((result) => {
                //     console.log(result); // thumbnail path
                //     thumbnail = result.path
                //     that.setState({
                //         thumbnail: result.path
                //     })
                //     AliyunOSS.asyncUpload(config.bucket, "/oss_demo/" + source.name, "file://" + result.path).then((res) => {
                //         console.log(res)
                //         Alert.alert(
                //             'Alert Title',
                //             "恭喜你成功上传到阿里云服务器",
                //             [
                //                 { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
                //                 { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                //                 { text: 'OK', onPress: () => console.log('OK Pressed') },
                //             ], { cancelable: false }
                //         )
                //     }).catch((error) => {
                //         console.log(error)
                //     })
                // })

            }
        });
    }
    render() {
        let that = this
        console.log(I18n.locale)
        const { selectedStatus } = this.state
        return (
            <View  style={{      flex: 1,
        position: 'relative'}}>
              <ScrollView
              onScroll={this.handleScroll}
              ref={(ref) => { this.scrollView = ref; }}
                contentInsetAdjustmentBehavior="automatic"
                scrollEnabled={!this.state.fullscreen }
                contentContainerStylee={{        alignItems: 'center',
                flex:1,
        justifyContent: 'center'}}
                style={{    position: 'absolute',

        top: 0,
        bottom: 0,
        zIndex:99,
        left: 0,
        right: 0, backgroundColor: this.state.fullscreen  ? "grey" : "transparent"}}
                >
                <View>
                  <Text style={styles.sectionTitle}>Webp使用实例 {API_ENV}： </Text>
                </View>

                <FastImage
                    style={styles.imageSquare}
                    source={require('../../images/account.webp')}
                />
                <View>
                  <Text style={styles.sectionTitle}>Teaset使用实例： </Text>
                </View>
                <View >
                  <Label size='xl' text='Hello world!' />
                  <Input style={{width: 200}}
                    size='lg'
                    value={this.state.value}
                    onChangeText={text => this.setState({value: text})}
                    />
                </View>
                <View>
                  <Text style={styles.sectionTitle}>I18n使用实例： </Text>
                </View>
                <View>
                  <Text>{I18n.t('greeting')}</Text>
                </View>
                <View>
                  <Text style={styles.sectionTitle}>Video使用实例： </Text>
                </View>
                <View>
                    <Demo />
                    <ImageList />
                </View>
                <View>
                  <Text style={styles.sectionTitle}>Content Loader使用实例： </Text>
                </View>
                <View>
                  <ContentLoader>
                    <Circle cx="30" cy="30" r="30" />
                    <Rect x="80" y="17" rx="4" ry="4" width="300" height="13" />
                    <Rect x="80" y="40" rx="3" ry="3" width="250" height="10" />
                  </ContentLoader>
                </View>
                <View>
                  <Text style={styles.sectionTitle}>Video Picker使用实例： </Text>
                </View>
                <TouchableOpacity onPress={this.selectVideoTapped}>
                  <View style={[styles.avatar, styles.avatarContainer]}>
                    <Text>Select a Video</Text>
                  </View>
                </TouchableOpacity>
                {this.state.fullscreen ? <View style={{height:this.state.videoViewHeight, width:this.state.videoViewWidth}}></View>:null}

                
                {this.state.videoSource && (
                 <View  style={ this.state.fullscreen ? [styles.fullscreenVideoPlayerStyle, {top:this.state.contentOffsetY}]  : styles.videoPlayerStyle}>
                    <VideoPlayer
                      endWithThumbnail
                      video={{ uri: this.state.videoSource }}
                      thumbnail={{ isStatic: true, uri: this.state.thumbnail }}
                      videoWidth={this.state.fullscreen ? screenWidth:this.state.video.width}
                      onToggleFullScreen={this.onToggleFullScreen}
                      onEnd={this.onEnd}
                       onLayout={(event) => {
                          var {x, y, width, height} = event.nativeEvent.layout;
                          console.log("height:" + height)
                          this.setState({
                              videoViewHeight:height,
                              videoViewWidth:width,
                          })
                        }} 
                      ref={r => this.player = r}
                    />

                 </View>
                )}
                {
                    this.state.showLottie ?
                    <LottieView source={require('../../animations/success.json')} autoPlay  />: null
                }
        <TouchableOpacity style={styles.login} onPress={() => this._onPressShare("wechat")}>
          <Text style={{ fontSize: 18, color: 'black' }}>分享微信</Text>

        </TouchableOpacity>


        <TouchableOpacity style={styles.login} onPress={() => this._onPressShare("qq")}>
          <Text style={{ fontSize: 18, color: 'black' }}>分享QQ</Text>

        </TouchableOpacity>

                <Toast
                    ref="toast"
                    style={{backgroundColor:'red'}}
                    position='top'
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{color:'black'}}
                />
              </ScrollView>


          </View>

        )
    }

}
const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.red,
        margin: 30,
    },
    fullscreenVideoPlayerStyle: {
        backgroundColor: "black",
        position: "absolute",
        left: 0,
        zIndex: 999,
        width: screenWidth,
        height: screenHeight,
        justifyContent: 'center',
        display: "flex",
    },
    videoPlayerStyle: {
        backgroundColor: "black"
    },
    avatarContainer: {
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        borderRadius: 75,
        width: 150,
        height: 150,
    },

    imageSquare: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 100,
        backgroundColor: '#fff',
        margin: 20,
        marginTop: 10,
        width: 100,
        flex: 1,
    },
    login: {
        width: 150,
        height: 60,
        borderColor: 'red',
        borderWidth: StyleSheet.hairlineWidth,
        borderRadius: 12,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },

    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 80,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'center',
    },
});