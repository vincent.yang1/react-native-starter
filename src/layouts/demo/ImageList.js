import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, FlatList, TouchableHighlight, SectionList, Dimensions, TouchableOpacity} from 'react-native';

const dimension = Dimensions.get('window')
class CellList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var theModel = this.props.itemModel
        return (
          <TouchableOpacity activeOpacity={0.8} onPress={()=>{alert('点击了')}} >
            <View style={styles.cellContainer}>
                <Image
                    source={{uri: theModel.iconUrl}}
                    style={styles.cellImg}/>
            </View>
          </TouchableOpacity>
        )
    }
}

class CellListOne extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var theModel = this.props.itemModel
        return (
          <TouchableOpacity activeOpacity={0.8} onPress={()=>{alert('点击了')}} >
            <View style={styles.cellContainerOne}>
                <Image
                    source={{uri: theModel.iconUrl}}
                    style={styles.cellImgOne}/>
            </View>
          </TouchableOpacity>
        )
    }
}

class CellListTwo extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var theModel = this.props.itemModel
        return (
          <TouchableOpacity activeOpacity={0.8} onPress={()=>{alert('点击了')}} >
            <View style={styles.cellContainerTwo}>
              <Image source={{uri: theModel.iconUrl}} style={styles.cellImgTwo}/>
            </View>
          </TouchableOpacity>
        )
    }
}


export default class TableListView extends Component {
    _renderItem = ({item}) => (
        <CellList
            itemModel = {item}
        />
    )
     _renderItemOne = ({item}) => (
        <CellListOne
            itemModel = {item}
        />
    )
     _renderItemTwo = ({item}) => (
        <CellListTwo
            itemModel = {item}
        />
    )
    _headerItem = () =>(
        <View style={styles.headerStyle}>
            <Text
                style={{fontSize:18.0}}
            >带你游艇嫩模</Text>
        </View>
    )
    render() {
        let data =[{
            "iconUrl":"https://img.52z.com/upload/news/image/20180212/20180212084623_32086.jpg",
        },
        {
           "iconUrl":"https://img.alicdn.com/tfscom/TB1zJPMtbSYBuNjSspfSuwZCpXa.jpg_80x80q90.jpg_.webp",
        // },
        // {
        //    "iconUrl":"https://img.alicdn.com/tfscom/TB1zJPMtbSYBuNjSspfSuwZCpXa.jpg_80x80q90.jpg_.webp",
        // },
        // {
        //    "iconUrl":"https://img.alicdn.com/tfscom/TB1zJPMtbSYBuNjSspfSuwZCpXa.jpg_80x80q90.jpg_.webp",
        // },
        // {
        //    "iconUrl":"https://img.alicdn.com/tfscom/TB1zJPMtbSYBuNjSspfSuwZCpXa.jpg_80x80q90.jpg_.webp",
        // },
        // {
        //    "iconUrl":"https://img.alicdn.com/tfscom/TB1zJPMtbSYBuNjSspfSuwZCpXa.jpg_80x80q90.jpg_.webp",
        // },
        // {
        //    "iconUrl":"https://img.alicdn.com/tfscom/TB1zJPMtbSYBuNjSspfSuwZCpXa.jpg_80x80q90.jpg_.webp",
        // },
        // {
        //    "iconUrl":"https://img.alicdn.com/tfscom/TB1zJPMtbSYBuNjSspfSuwZCpXa.jpg_80x80q90.jpg_.webp",
        // },
        // {
        //     "iconUrl":"https://img.52z.com/upload/news/image/20180212/20180212084623_32086.jpg",
        }]


        if(data.length == 1) {
          return (
            <View>
              <FlatList
                  renderItem={this._renderItemOne}
                  data={data}
                  numColumns={1}
                  keyExtractor={(item, index) => item + index}
                  ListHeaderComponent={this._headerItem}
              />
            </View>
          );
        } else if(data.length == 2 || data.length == 4) {
          return (
            <View>
              <FlatList
                  renderItem={this._renderItemTwo}
                  data={data}
                  numColumns={2}
                  keyExtractor={(item, index) => item + index}
                  ListHeaderComponent={this._headerItem}
              />
            </View>
          );
        } else if(data.length > 9) {
          data = data.splice(0,9)
        }

        return (
            <View>
                <FlatList
                    renderItem={this._renderItem}
                    data={data}
                    numColumns={3}
                    keyExtractor={(item, index) => item + index}
                    ListHeaderComponent={this._headerItem}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
  headerStyle:{
    marginTop:20.0,
    marginBottom:10.0,
    height:50.0,
    justifyContent:'center',
    backgroundColor:'lightgray'
  },
  cellContainer: {
    borderColor:'#ccc',
    borderWidth:1,
    padding:10,
    marginTop:10,
    marginRight:10,
    alignItems:'center',
    width:(dimension.width-40-40)/3
  },
  cellContainerOne: {
    borderColor:'#ccc',
    borderWidth:1,
    padding:10,
    marginTop:10,
    flex:1,
    alignItems:'center',
  },

  cellContainerTwo: {
    borderColor:'#ccc',
    borderWidth:1,
    padding:10,
    marginTop:10,
    marginRight:10,
    alignItems:'center',
    width:(dimension.width-40-30)/2,
    height:(dimension.width-40-30)/2
  },
  cellTitleViewStyle:{
    justifyContent: "center",

  },
  cellImgTwo: {
    width: '100%',
    height: '100%',
    borderRadius: 40.0,
  },
  cellImgOne: {
    width: '100%',
    height: '100%',
    minHeight:200,
    borderRadius: 40.0,
  },
  cellImg: {
    width: '100%',
    height:80,
    borderRadius: 40.0,
  },
})




