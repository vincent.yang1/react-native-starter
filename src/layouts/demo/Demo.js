import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
  Modal,
} from 'react-native';
import DeviceInfo from 'react-native-device-info'
import SplashScreen from 'react-native-splash-screen'
import ViewPager from '@react-native-community/viewpager';
import ImageViewer from 'react-native-image-zoom-viewer';
import CameraRoll from "@react-native-community/cameraroll";
import RNModal from "react-native-modal";

var RNFS = require('react-native-fs');
const styles = StyleSheet.create({
  viewPager: {
    flex: 1,
    height:200,
  },
  swipeImg: {
    width: '100%',
    height:200,
  }
});

const images = [{
    // Simplest usage.
    url: 'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460',

    // width: number
    // height: number
    // Optional, if you know the image size, you can set the optimization performance

    // You can pass props to <Image />.
    props: {
        // headers: ...
    }
}, {
    url: 'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460',
    // props: {
    //     // Or you can set source directory.
    //     source: require('../background.png')
    // }
}]



class Demo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userLocaleCountryCode:'',
      visible:false
    }
  }

  componentDidMount() {
    let that = this
    SplashScreen.hide();
    DeviceInfo.getAndroidId().then(androidId => {
      that.setState({
        userLocaleCountryCode:androidId
      })
    });
  }


  downLoadImg(formUrl) {
    let that = this

    let downloadDest = `${RNFS.DocumentDirectoryPath}/${((Math.random() * 1000) | 0)}.jpg`;

    const options = {
      fromUrl: formUrl,
      toFile: downloadDest,
      background: true,
      begin: (res) => {
          console.log('begin', res);
          console.log('contentLength:', res.contentLength / 1024 / 1024, 'M');
      },
      progress: (res) => {
      }
    };

    try {
        const ret = RNFS.downloadFile(options);
        ret.promise.then(res => {
          alert('downloadDest '+ downloadDest)
            console.log('success', res);
            console.log('file://' + downloadDest)
            CameraRoll.saveToCameraRoll(downloadDest)
                .then(()=>{
                    alert('save success')
                }).catch(()=>{
                    alert('failed')
                })
        }).catch(err => {
            console.log('err', err);
        });
    }
    catch (e) {
        console.log(error);
    }
  }


  render() { 
    const {userLocaleCountryCode, visible } = this.state
    let iconUrl="https://img.52z.com/upload/news/image/20180212/20180212084623_32086.jpg"
    let that = this
    return (
      <View>
        
        <Text>滑动实例</Text>
        <ViewPager style={styles.viewPager} initialPage={0}>
          <View key="1">
            <TouchableOpacity activeOpacity={0.8}  onPress={() => this.downLoadImg(iconUrl)} >
              <Image source={{uri: iconUrl}} style={styles.swipeImg}/>
              <Text>First page</Text>
            </TouchableOpacity>
          </View>
          <View key="2">
            <Image source={{uri: iconUrl}} style={styles.swipeImg}/>
            <Text>Second page</Text>
          </View>
          <View key="3">
            <Image source={{uri: iconUrl}} style={styles.swipeImg}/>
            <Text>Second page</Text>
          </View>
          <View key="4">
            <Image source={{uri: iconUrl}} style={styles.swipeImg}/>
            <Text>Second page</Text>
          </View>
        </ViewPager>
        <Text>device-info 实例2，Android id is :{userLocaleCountryCode}</Text>

        <TouchableOpacity activeOpacity={0.8} onPress={()=>{this.setState({visible:true})}} >
          <Text>
              显示滑动大图
          </Text>
        </TouchableOpacity>
        <RNModal 
          visible={visible} 
          transparent={visible} 
         
        >
            
              <ImageViewer imageUrls={images} onClick={()=>{this.setState({visible:false})}}/>
            
          </RNModal>
      </View>
    );
  }
};



export default Demo;
