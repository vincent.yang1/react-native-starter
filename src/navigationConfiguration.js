/**
 */

import React from 'react'


import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import App from './layouts/main/App';



const routeConfiguration = {
    App: {screen: App},
};

const stackNavigatorConfiguration = {
    headerMode: 'none',
    mode: 'card',
    initialRouteName: 'App',
    defaultNavigationOptions: {
        gesturesEnabled: true
    }
}


const RootStack = createStackNavigator(routeConfiguration, stackNavigatorConfiguration)
export const AppNavigator = createAppContainer(RootStack);
