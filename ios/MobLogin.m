//
//  MobLogin.m
//  rnsharesdk
//
//  Created by cc on 2017/2/1.
//  Copyright © 2017年 qq. All rights reserved.
//
#import "MobLogin.h"

@implementation MobLogin

RCT_EXPORT_MODULE();

- (instancetype)init
{
    if(self = [super init]){
        NSLog(@"initShareSdk()!");
        /**
         *  设置ShareSDK的appKey，如果尚未在ShareSDK官网注册过App，请移步到http://mob.com/login 登录后台进行应用注册，
         *  在将生成的AppKey传入到此方法中。我们Demo提供的appKey为内部测试使用，可能会修改配置信息，请不要使用。
         *  方法中的第二个参数用于指定要使用哪些社交平台，以数组形式传入。第三个参数为需要连接社交平台SDK时触发，
         *  在此事件中写入连接代码。第四个参数则为配置本地社交平台时触发，根据返回的平台类型来配置平台信息。
         *  如果您使用的时服务端托管平台信息时，第二、四项参数可以传入nil，第三项参数则根据服务端托管平台来决定要连接的社交SDK。
         */
        [ShareSDK registPlatforms:^(SSDKRegister *platformsRegister) {
            //QQ
            [platformsRegister setupQQWithAppId:@"100371282" appkey:@"aed9b0303e3ed1e27bae87c33761161d"];            
            //微信
            [platformsRegister setupWeChatWithAppId:@"wx617c77c82218ea2c" appSecret:@"c7253e5289986cf4c4c74d1ccc185fb1" universalLink:@"https://www.sandslee.com/"];            
            //新浪
            [platformsRegister setupSinaWeiboWithAppkey:@"568898243" appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3" redirectUrl:@"http://www.sharesdk.cn"];
            
            //Facebook
            //[platformsRegister setupFacebookWithAppkey:@"328083627894116" appSecret:@"847ca6e8e1a1339100c52dfe68f89577" displayName:@"shareSDK"];
            [platformsRegister setupFacebookWithAppkey:@"328083627894116" appSecret:@"847ca6e8e1a1339100c52dfe68f89577" displayName:@"dipbit"];
            //Twitter
            [platformsRegister setupTwitterWithKey:@"viOnkeLpHBKs6KXV7MPpeGyzE" secret:@"NJEglQUy2rqZ9Io9FcAU9p17omFqbORknUpRrCDOK46aAbIiey" redirectUrl:@"http://mob.com"];
            //Instagram
            [platformsRegister setupInstagramWithClientId:@"ff68e3216b4f4f989121aa1c2962d058" clientSecret:@"1b2e82f110264869b3505c3fe34e31a1" redirectUrl:@"http://sharesdk.cn"];
            
            //钉钉
            [platformsRegister setupDingTalkWithAppId:@"dingoabcwtuab76wy0kyzo"];
            
            //支付宝
            [platformsRegister setupAliSocialWithAppId:@"2017062107540437"];
            
            //豆瓣
            [platformsRegister setupDouBanWithApikey:@"02e2cbe5ca06de5908a863b15e149b0b" appSecret:@"9f1e7b4f71304f2f" redirectUrl:@"http://www.sharesdk.cn"];
            
            //Dropbox
            [platformsRegister setupDropboxWithAppKey:@"us514wslpfojbxc" appSecret:@"w0nmp4os3ngo1ja" redirectUrl:@"http://localhost"];
            
            //易信
            [platformsRegister setupYiXinByAppId:@"yx0d9a9f9088ea44d78680f3274da1765f" appSecret:@"1a5bd421ae089c3" redirectUrl:@"https://open.yixin.im/resource/oauth2_callback.html"];
            //Flickr
            [platformsRegister setupFlickrWithApiKey:@"cbed81d4a1bc7417693ab7865e354717" apiSecret:@"4c490343869091f2"];
            
            //Instapaper
            [platformsRegister setupInstapaperWithConsumerKey:@"4rDJORmcOcSAZL1YpqGHRI605xUvrLbOhkJ07yO0wWrYrc61FA" consumerSecret:@"GNr1GespOQbrm8nvd7rlUsyRQsIo3boIbMguAl9gfpdL0aKZWe"];
            
            //Line
            [platformsRegister setupLineAuthType:SSDKAuthorizeTypeBoth];
            
            //YinXiang
            [platformsRegister setupEvernoteByConsumerKey:@"46131514-6903" consumerSecret:@"08d7a6f3afcc888a" sandbox:YES];
            
            //Evernote
            [platformsRegister setupEvernoteByConsumerKey:@"46131514-6903" consumerSecret:@"08d7a6f3afcc888a" sandbox:YES];
            
            //kakao
            [platformsRegister setupKaKaoWithAppkey:@"9c17eb03317e0e627ec95a400f5785fb" restApiKey:@"802e551a5048c3172fc1dedaaf40fcf1" redirectUrl:@"http://www.mob.com/oauth"];
            
            //VKontakte
            [platformsRegister setupVKontakteWithApplicationId:@"5312801" secretKey:@"ZHG2wGymmNUCRLG2r6CY" authType:SSDKAuthorizeTypeBoth];
            
            //YouTube
            [platformsRegister setupYouTubeWithClientId:@"906418427202-jinnbqal1niq4s8isbg2ofsqc5ddkcgr.apps.googleusercontent.com" clientSecret:@"" redirectUrl:@"http://localhost"];
            
            //美拍
            [platformsRegister setupMeiPaiWithAppkey:@"1089867639"];
            
            //腾讯微博
            [platformsRegister setupTencentWeiboWithAppkey:@"801307650" appSecret:@"ae36f4ee3946e1cbb98d6965b0b2ff5c" redirectUrl:@"http://www.sharesdk.cn"];
            
            //人人网
            [platformsRegister setupRenRenWithAppId:@"226427" appKey:@"fc5b8aed373c4c27a05b712acba0f8c3" secretKey:@"f29df781abdd4f49beca5a2194676ca4" authType:SSDKAuthorizeTypeBoth];
            
            //CMCC
            [platformsRegister setupCMCCByAppId:@"300011862498" appKey:@"38D9CA1CC280C5F207E2C343745D4A4B" displayUI:YES];
            
            //有道云笔记
            [platformsRegister setupYouDaoNoteWithConsumerKey:@"dcde25dca105bcc36884ed4534dab940" consumerSecret:@"d98217b4020e7f1874263795f44838fe" oauthCallback:@"http://www.sharesdk.cn/"];
            
            //明道
            [platformsRegister setupMingDaoByAppKey:@"97230F25CA5C" appSecret:@"A5DC29AF7C5A5851F28E903AE9EAC0" redirectUrl:@"http://mob.com"];
            
            //开心网
            [platformsRegister setupKaiXinByApiKey:@"358443394194887cee81ff5890870c7c" secretKey:@"da32179d859c016169f66d90b6db2a23" redirectUrl:@"http://www.sharesdk.cn/"];
            
            //Google+
            [platformsRegister setupGooglePlusByClientID:@"351114257251-isfr7cnt5gop930krntpf246d9ofv8j5.apps.googleusercontent.com" clientSecret:@"" redirectUrl:@"http://localhost"];
            
            //Pinterest
            [platformsRegister setupPinterestByClientId:@"4987008320438021391"];
            
            //Pocket
            [platformsRegister setupPocketWithConsumerKey:@"11496-de7c8c5eb25b2c9fcdc2b627" redirectUrl:@"pocketapp1234"];
            
            //LinkedIn
            [platformsRegister setupLinkedInByApiKey:@"46kic3zr7s4n" secretKey:@"RWw6WRl9YJOcdWsj" redirectUrl:@"http://baidu.com"];
            
            //Instapaper
            [platformsRegister setupInstapaperWithConsumerKey:@"4rDJORmcOcSAZL1YpqGHRI605xUvrLbOhkJ07yO0wWrYrc61FA" consumerSecret:@"GNr1GespOQbrm8nvd7rlUsyRQsIo3boIbMguAl9gfpdL0aKZWe"];
            
            //Tumblr
            [platformsRegister setupTumblrByConsumerKey:@"2QUXqO9fcgGdtGG1FcvML6ZunIQzAEL8xY6hIaxdJnDti2DYwM" consumerSecret:@"3Rt0sPFj7u2g39mEVB3IBpOzKnM3JnTtxX2bao2JKk4VV1gtNo" redirectUrl:@"http://sharesdk.cn"];
            
            //SMS
            [platformsRegister setupSMSOpenCountryList:NO];
            
            //Telegram
            [platformsRegister setupTelegramByBotToken:@"600852601:AAElp9J93JiYevLocDIEYPhEYulnMFuB_nQ" botDomain:@"http://127.0.0.1"];
            
            //Reddit
            [platformsRegister setupRedditByAppKey:@"ObzXn50T7Cg0Xw" redirectUri:@"https://www.mob.com/reddit_callback"];
            
            //ESurfing(天翼)
            [platformsRegister setupESurfingByAppKey:@"8148612606" appSecret:@"mCltrhUqwshFa86egDTs0491ibaAulKA" appName:@"shareSDK"];
        } ];
    }
    return self;
}

RCT_EXPORT_METHOD(showShare:(NSInteger *)platform  :(NSString *)title :(NSString *)content :(NSString *)url :(NSString *)imgUrl callback:(RCTResponseSenderBlock)callback) {
    dispatch_async(dispatch_get_main_queue(), ^{
        //1、创建分享参数（必要）
        //        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        NSArray* imageArray = @[imgUrl];
        //自动适配类型，视传入的参数来决定
        NSInteger*  type = 0;
        if(platform == 24 && [imgUrl length] != 0) {
            type = 2; // 图片
        }
        //        NSArray* items = nil;
        //  // 设置分享菜单栏样式（非必要）
        //    // 设置分享菜单的背景颜色
        //    [SSUIShareActionSheetStyle setActionSheetBackgroundColor:[UIColor colorWithRed:249/255.0 green:0/255.0 blue:12/255.0 alpha:0.5]];
        // 设置分享菜单颜色
        //创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:content
                                         images:imageArray //传入要分享的图片
                                            url:[NSURL URLWithString:url]
                                          title:title
                                           type:type];
        
        //进行分享
        [ShareSDK share:platform //传入分享的平台类型
             parameters:shareParams
         onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
             switch (state) {
                 case SSDKResponseStateSuccess:
                     NSLog(@"分享成功!");
                     callback(@[[NSNull null], @"分享成功"]);
                     break;
                 case SSDKResponseStateFail:
                     NSLog(@"分享失败%@",error);
                     callback(@[[NSString stringWithFormat:@"%@", error], @"分享失败"]);
                     break;
                 case SSDKResponseStateCancel:
                     NSLog(@"分享已取消");
                     break;
                 default:
                     break;
             }
         }];
        
        
        
        
        
        //
        //       items = @[
        //                 @(SSDKPlatformSubTypeWechatSession),
        //                 @(SSDKPlatformTypeSinaWeibo),
        //                 @(SSDKPlatformTypeFacebook),
        //                 @(SSDKPlatformTypeTwitter),
        //                 @(SSDKPlatformSubTypeQQFriend)
        //                 ];
        //        [shareParams SSDKSetupShareParamsByText:content
        //                                         images:imageArray
        //                                            url:[NSURL URLWithString:url]
        //                                          title:title
        //                                           type:SSDKContentTypeAuto];
        //        //2、分享（可以弹出我们的分享菜单和编辑界面）
        //        [ShareSDK showShareActionSheet:nil //要显示菜单的视图, iPad版中此参数作为弹出菜单的参照视图，只有传这个才可以弹出我们的分享菜单，可以传分享的按钮对象或者自己创建小的view 对象，iPhone可以传nil不会影响
        //                                 items:items
        //                           shareParams:shareParams
        //                   onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
        //
        //                       switch (state) {
        //                           case SSDKResponseStateSuccess:
        //                           {
        //                               break;
        //                           }
        //                           case SSDKResponseStateFail:
        //                           {
        //                               break;
        //                           }
        //                           default:
        //                               break;
        //                       }
        //                   }
        //         ];
    });
}

@end


